$(function onClickSetCurrentUmidity() {
  $("#btn-collapse-umidity").click(function() {
    $.ajax({
        url : 'http://127.0.0.1:4040/',
        dataType: 'text',
        error : function() {
          $.ajax({
            url : 'http://ftp.sghmobile.altervista.org/history.json',
            dataType : 'text',
            success : function (res) {
              var humidity = JSON.parse(res);
              var humidity_val = humidity.humidity[humidity.humidity.length-1].value;
              $("#current-umidity").html("Non riesco a rilevare nessun dispositivo connesso ai sensori, ultimo valore rilevato: " + humidity_val + "%");
            }
          });
        },
        success : function(res) {
          var humidity = JSON.parse(res);
          var humidity_val = humidity.humidity[humidity.humidity.length-1].value;
          $("#current-umidity").html(humidity_val + "%");
        }
    });
  });

  $("#btn-collapse-umidity2").click(function() {
    $.ajax({
        url : 'http://127.0.0.1:4040/',
        dataType: 'text',
        success : function(res) {
          var humidity = JSON.parse(res);
          var humidity_val = humidity.humidity[humidity.humidity.length-1].value;
          $("#current-umidity").html(humidity_val + "%");
        }
    });
  });

  $("#connect-now").click(function() {
    var connect = Android.canConnectToDevice();
    if(connect){
      window.location = "ConnectedApp.html";
    } else {
      Android.cannotConnectToast("Connessione fallita!");
    }
  })

  $("#checkbox-1").change(function() {
    if($("#checkbox-1").prop("checked")){
      if($("#customRange2").val() == 0) {
        $("#customRange2").prop("value", 1);
      }
    } else {
      if($("#customRange2").val() > 0) {
        $("#customRange2").prop("value", 0);
      }
    }
  });

  $("#customRange2").change(function() {
    Android.setPumpIntensity($("#customRange2").val());
    if($("#customRange2").val() > 0){
      $("#checkbox-1").prop("checked", true);
    } else if ($("#customRange2").val() == 0) {
      $("#checkbox-1").prop("checked", false);
    }
  });
});
