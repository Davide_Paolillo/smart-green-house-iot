package status;

public class StatusUtils {
	
	private static Status status;
	
	public static void setState(Status s) {
		status = s;
	}
	
	public static Status getState() {
		return status;
	}

}
