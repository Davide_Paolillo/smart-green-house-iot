package arduino;

public class Irrigation implements Event {
	
	private String pump;
	
	public Irrigation(String pump){
		this.pump = pump;
	}
	
	public String getPumpIrrigation(){
		return this.pump;
	}

}
