package arduino;

public interface Observer {

	boolean notifyEvent(Event ev);
}
