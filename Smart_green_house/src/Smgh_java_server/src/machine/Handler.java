package machine;

import arduino.Event;

public interface Handler {
	void handle(Event ev);
}
