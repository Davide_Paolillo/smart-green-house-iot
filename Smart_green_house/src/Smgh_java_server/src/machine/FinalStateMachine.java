package machine;

import org.json.JSONException;

import arduino.AutoMode;
import arduino.Event;
import arduino.Humidity;
import arduino.Irrigation;
import arduino.ManualMode;
import arduino.ObservableTimer;
import arduino.Tick;
import esp_comunication.DataService;
import json_management.HumidityManager;
import json_management.IrrigationManager;
import json_management.SegnalationManager;
import json_management.StateManager;
import serial_comunication.MessageManager;
import status.*;

public class FinalStateMachine extends EventLoopControllerWithHandlers{
	
	private MessageManager msg;
	private DataService server;
	private Status current;
	private ObservableTimer timer;
	
	private static final int MIN = 30;
	private static final int MED = 60;
	private static final int MAX = 90;
	
	
	public FinalStateMachine(DataService server, MessageManager message) {
		this.server = server;
		this.msg = message;
		this.timer = new ObservableTimer();
		this.startObserving(this.server);
		this.startObserving(this.msg);
		this.startObserving(this.timer);
	}

	@Override
	protected void setupHandlers() {
		addHandler(Humidity.class, (Event ev) -> {			
			try {
				HumidityManager.writeHumidity(((Humidity) ev).getHumidity());
				if(!this.current.equals(Status.MANUAL)) {
					if(((Humidity) ev).getHumidity() < 30 && ((Humidity) ev).getHumidity() > 20) {
						this.msg.sendMassage(Integer.toString(MIN));
						IrrigationManager.writeIrrigation(MIN);
						timer.start(6500);
						this.current = Status.ON;
						
					}
					else if(((Humidity) ev).getHumidity() < 20 && ((Humidity) ev).getHumidity() > 10) {
						this.msg.sendMassage(Integer.toString(MED));
						IrrigationManager.writeIrrigation(MED);
						timer.start(6500);
						this.current = Status.ON;
						
					}
					else if(((Humidity) ev).getHumidity() < 10) {
						this.msg.sendMassage(Integer.toString(MAX));
						IrrigationManager.writeIrrigation(MAX);
						timer.start(6500);
						this.current = Status.ON;
					}
					else if(((Humidity) ev).getHumidity() > 35 && this.current.equals(Status.ON)) {
						timer.stop();
					}
				}	
				else {
					this.msg.sendMassage(Integer.toString(((Humidity) ev).getHumidity()));
				}
			}
			catch (Exception ex) {}
		}).addHandler(Tick.class, (Event ev) -> {
			try {
				SegnalationManager.writeSegnalation();
			}
			catch (Exception ex) {}
		}).addHandler(Irrigation.class, (Event ev) -> {
			if(this.current.equals(Status.MANUAL)){
				try {
					IrrigationManager.writeIrrigation(Integer.parseInt(((Irrigation) ev).getPumpIrrigation()));
				} catch (NumberFormatException | JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				timer.start(6500);
			}
			
		}).addHandler(ManualMode.class, (Event ev) -> {
			this.current = Status.MANUAL;
			try {
				StateManager.writeState("manual");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).addHandler(AutoMode.class, (Event ev) -> {
			this.current = Status.OFF; 
			try {
				StateManager.writeState("auto");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}

}
