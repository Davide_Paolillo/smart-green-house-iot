package serial_comunication;

import arduino.AutoMode;
import arduino.Irrigation;
import arduino.ManualMode;
import arduino.ObservableImpl;

public class MessageManager extends ObservableImpl{
	
	private SerialCommChannel channel;
	
	public MessageManager(SerialCommChannel channel) {
		this.channel = channel;
	}
	
	public synchronized void manageMessage() throws InterruptedException {
		if(channel.isMsgAvailable()) {
			String msg = channel.receiveMsg();
			if(msg.equals("m")) {
				this.notifyEvent(new ManualMode());
			}
			else if(msg.equals("a")) {
				this.notifyEvent(new AutoMode());
			}
			else {
				this.notifyEvent(new Irrigation(msg));
			}
		}	
	}
	
	public synchronized void sendMassage(String msg) {
		this.channel.sendMsg(msg);
	}	

}
