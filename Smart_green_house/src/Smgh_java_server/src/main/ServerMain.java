package main;

import esp_comunication.DataService;
import io.vertx.core.Vertx;
import machine.FinalStateMachine;
import serial_comunication.MessageManager;
import serial_comunication.SerialCommChannel;
import status.*;

public class ServerMain {
	
	public static void main(String[] args) throws Exception {
		
		StatusUtils.setState(Status.OFF);
		Vertx vertx = Vertx.vertx();
		DataService service = new DataService(8080);
		vertx.deployVerticle(service);
		SerialCommChannel serial = new SerialCommChannel("COM5", 9600);
		MessageManager message = new MessageManager(serial);
		new FinalStateMachine(service, message).start();
		
	}
	
}
