function sendData(content) {
  var options = {
    host: '496ccad1.ngrok.io',
    port: '80',
    path:'/api/data',
    method:'POST',
    headers: {
      "Content-Type":"application/json",
      "Content-Length":content.length
    }
  };
  console.log("sending " + content + "...");
  require("http").request(options, function(res)  {
    res.on('close', function(data) {
      console.log("OK.");
    });
  }).end(content);
};

function doJob(){
  setInterval(function(){
    var value = analogRead(A0)/1023.0;
    sendData('{ "value": ' + value + ', "place": "home" }');
  },2000);
}

var wifi = require("Wifi");

wifi.connect("my_connection", {password:"asd123"}, function(err){
  if (err==null){
    console.log("connected: ",wifi.getIP());
    doJob();
  } else {
    console.log("error - not connected.");
  }
});


/*
ERRATA
$.ajax({
    type: 'GET',
    url: 'http://ftp.sghmobile.altervista.org/smgh.json',
    data: { get_param: 'value' }, // dati da inviare al server
    dataType: 'json',
    success: function (data) {
    }
}); */

/*
CORRETTA
1. var data = $.ajax({
    url: 'ftp://sghmobile@ftp.sghmobile.altervista.org/smgh.json',
    async: false,
    dataType: 'json'
}).responseJSON;

2. var markers = [{ "position": "128.3657142857143", "markerPosition": "7" },
               { "position": "235.1944023323615", "markerPosition": "19" },
               { "position": "42.5978231292517", "markerPosition": "-3" }];

$.ajax({
    type: "POST",
    url: "ftp://sghmobile@ftp.sghmobile.altervista.org/smgh.json",
    // The key needs to match your method's input parameter (case-sensitive).
    data: JSON.stringify(markers), // markers e' una var, ne devo creare una mia e cambiare il nome
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function(data){alert(data);},
    failure: function(errMsg) {
        alert(errMsg);
    }
});
*/
