package smarthouse.iot.uni.smart_green_house_app;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.ParcelUuid;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class BTUtils {
    private BluetoothAdapter btadapter;
    private Context c;
    private Set<BluetoothDevice> paired_devices;
    private OutputStream outputStream;
    private InputStream inStream;
    private static final int first = 0;

    BTUtils(Context c, BluetoothAdapter bta){
        this.c = c;
        this.btadapter = bta;
    }

    public void init() throws IOException {
        if(checkBlueToothAdapter()){
            findPairedDevices();
        }
    }

    private boolean checkBlueToothAdapter(){
        if(this.btadapter == null){
            Toast.makeText(c.getApplicationContext(), "Your device does not support BlueTooth", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private void findPairedDevices() throws IOException {
        this.paired_devices = this.btadapter.getBondedDevices();
        if(paired_devices.size() > 0){
            Object[] devices = (Object []) paired_devices.toArray();
            BluetoothDevice device = (BluetoothDevice) devices[first];
            ParcelUuid[] uuids = device.getUuids();
            BluetoothSocket socket = device.createRfcommSocketToServiceRecord(uuids[0].getUuid());
            socket.connect();
            outputStream = socket.getOutputStream();
            inStream = socket.getInputStream();
        } /*else {
            Toast.makeText(c.getApplicationContext(), "Connect to arduino BlueTooth before this action", Toast.LENGTH_LONG).show();
        }*/
    }

    // Function to call when need to write in output
    public void write(String s) throws IOException {
        outputStream.write(s.getBytes());
    }

    // Function to call when need to read in input
    public void run() {
        final int BUFFER_SIZE = 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytes = 0;
        int b = BUFFER_SIZE;

        while (true) {
            try {
                bytes = inStream.read(buffer, bytes, BUFFER_SIZE - bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
