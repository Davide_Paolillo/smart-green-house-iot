package smarthouse.iot.uni.smart_green_house_app;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.io.IOException;

public class WebAppInterface {
    Context mContext;
    private BluetoothAdapter bluetoothAdapter;
    private int humidity;
    private boolean connection;
    private int intensity;
    private BTUtils bt;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
        this.humidity = 0;
        this.connection = false;
        this.intensity = 0;
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.bt = new BTUtils(this.mContext, this.bluetoothAdapter);
        try {
            this.bt.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public int getHumidity() {
        return humidity;
    }

    @JavascriptInterface
    public boolean canConnectToDevice(){
        return this.connection;
    }

    @JavascriptInterface
    public void cannotConnectToast(String msg) {
        Toast.makeText(mContext.getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }

    @JavascriptInterface
    public void setPumpIntensity(int value) {
        this.intensity = value;
        if (this.intensity >= 0) {
            try {
                this.bt.write(Integer.toString(this.intensity));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setHumidity(int humidity){
        this.humidity = humidity;
    }

    public void setConnectionFlag(boolean input){
        this.connection = input;
    }

    public int getPumpIntensity(){
        return this.intensity;
    }
}