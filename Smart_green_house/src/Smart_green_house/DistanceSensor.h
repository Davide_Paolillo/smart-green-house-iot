#ifndef __DISTANCE_SENSOR__
#define __DISTANCE_SENSOR__

class DistanceSensor {

  public:
    virtual long getSensorDistance();

};

#endif  
