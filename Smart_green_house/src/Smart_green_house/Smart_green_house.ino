#include <Arduino.h>
#include <ServoTimer2.h>
#include <SoftwareSerial.h>
#include "Scheduler.h"
#include "DetectModality.h"
#include "ManualMode.h"
#include "AutoMode.h"

#define TX 1
#define RX 2
#define L1 4
#define L2 5
#define L3 6
#define SERVO 8
#define TRIGGER 12
#define ECHO 13

Scheduler sched;
ServoTimer2 my_servo;

void setup() {
  // Declaration of components
  SoftwareSerial bluetooth(RX, TX);
  // Initialization of ports and scheduler
  Serial.begin(9600);
  bluetooth.begin(9600);
  sched.init(5000);
  my_servo.attach(SERVO);
  //Tasks istantiation
  Task* t0 = new DetectModality(L1, ECHO, TRIGGER, &bluetooth);
  Task* t1 = new ManualMode(L3, ECHO, TRIGGER, &bluetooth, &my_servo);
  Task* t2 = new AutoMode(L1, L2, &my_servo);
  // Tasks initialization
  t0->init(1000);
  t1->init(1000);
  t2->init(5000);
  // Adding tasks to scheduler
  sched.addTask(t0);
  sched.addTask(t1);
  sched.addTask(t2);
}

void loop() {
  // Running the scheduler in loop
  sched.run();
}
