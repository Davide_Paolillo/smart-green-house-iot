#ifndef __BT_SENSOR_IMPL__
#define __BT_SENSOR_IMPL__

#include <Arduino.h>
#include "BlueToothSensor.h"
#include "DistanceSensorImpl.h"
#include "SoftwareSerial.h"

#define MIN_DIST 0.3

class BlueToothSensorImpl: public BlueToothSensor {
public:
  BlueToothSensorImpl(int echo, int trigger, SoftwareSerial* bt);
  bool IsBlueToothAvailable();
  String BlueToothMessage();
  bool BlueToothWrite(int out);
private:
  int echo, trigger;
  DistanceSensorImpl* ds;
  SoftwareSerial* bluetooth;
};



#endif
