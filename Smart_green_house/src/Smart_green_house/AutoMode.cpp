#include "AutoMode.h"
#include "DetectModality.h"

#define PUMP_MIN_ACTIVATION_CODE 30
#define MAX_U 35.0
#define MIN_INTENSITY 64
#define MEDIUM_INTENSITY 128
#define MAX_INTENSITY 255
#define NUM_CYCLE 5000
#define _DELAY 1000
#define MIN_CYCLE_ADDITION 0.002
#define MED_CYCLE_ADDITION 0.004
#define MAX_CYCLE_ADDITION 0.006
#define SERVO_ZERO 750
#define SERVO_MIN 1000
#define SERVO_MED 1250
#define SERVO_MAX 1500

AutoMode::AutoMode (int led_1, int led_2, ServoTimer2* my_servo) {
  this->my_servo = my_servo;
  this->led_1 = new Led(led_1);
  this->led_2 = new Led(led_2);
}

void AutoMode::init(int period){
  Task::init(period);
}

void AutoMode::tick(){
  if(Serial.available()){
    int incomingByte = Serial.read();
    if (incomingByte < PUMP_MIN_ACTIVATION_CODE) {
      led_1->switchOff();
      if (incomingByte >= 20) {
        led_2->swithchOnWithIntensity(MIN_INTENSITY);
        my_servo->write(SERVO_MIN);
        Serial.println(MIN_INTENSITY);
      } else if (incomingByte >= 10 && incomingByte < 20) {
        my_servo->write(SERVO_MED);
        led_2->swithchOnWithIntensity(MEDIUM_INTENSITY);
        Serial.println(MEDIUM_INTENSITY);
      } else if (incomingByte < 10) {
        my_servo->write(SERVO_MAX);
        led_2->swithchOnWithIntensity(MAX_INTENSITY);
        Serial.println(MAX_INTENSITY);
      }
    } else {
      my_servo->write(SERVO_ZERO);
    }
  }
}

bool AutoMode::isMyTurn(){
  return auto_mode;
}
