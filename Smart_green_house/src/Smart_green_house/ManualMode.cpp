#include "Arduino.h"
#include "ManualMode.h"
#include "DetectModality.h"

#define SERVO_ZERO 750
#define SERVO_MIN 1000
#define SERVO_MED 1250
#define SERVO_MAX 1500

ManualMode::ManualMode (int led_3, int echo, int trigger, SoftwareSerial* bt, ServoTimer2* my_servo) {
  this->led_3 = led_3;
  this->echo = echo;
  this->trigger = trigger;
  this->bt = bt;
  this->my_servo = my_servo;
  bs = new BlueToothSensorImpl(this->echo, this->trigger, this->bt);
  led = new Led(led_3);
}

void ManualMode::init(int period){
  Task::init(period);
}

void ManualMode::tick(){
  /// Se bluetooth e' available allora auto_mode = false, manual_mode = true
  if(bs->IsBlueToothAvailable()){
    led->switchOn();
    message = bs->BlueToothMessage();
    if (message.toInt() >= 0 || message.toInt() <= 3) {
      switch (message.toInt()) {
        case 0:
          my_servo->write(SERVO_ZERO);
        case 1:
          my_servo->write(SERVO_MIN);
        case 2:
          my_servo->write(SERVO_MED);
        case 3:
          my_servo->write(SERVO_MAX);
      }
      if(Serial.available()){
        Serial.println(message.toInt());
      }
    }
  } else {
    led->switchOff();
    manual_mode = false;
    auto_mode = true;
  }
}

bool ManualMode::isMyTurn(){
  return manual_mode;
}
