#ifndef __AUTO_MODE__
#define __AUTO_MODE__

#include <Arduino.h>
#include <ServoTimer2.h>
#include "Task.h"
#include "Led.h"

class AutoMode: public Task {
public:
  AutoMode(int led_1, int led_2, ServoTimer2* my_servo);
  void init(int period);
  void tick();
  bool isMyTurn();
private:
  Led* led_1;
  Led* led_2;
  ServoTimer2* my_servo;
};

#endif
