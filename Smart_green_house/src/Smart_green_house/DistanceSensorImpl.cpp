#include "DistanceSensorImpl.h"
#include "Arduino.h"

DistanceSensorImpl::DistanceSensorImpl (int triggerPort, int echoPort) {
  this->triggerPort = triggerPort;
  this->echoPort = echoPort;
  pinMode(triggerPort, OUTPUT);
  pinMode(echoPort, INPUT);
}

long DistanceSensorImpl::getSensorDistance () {
  digitalWrite(triggerPort, LOW);
  delayMicroseconds(10);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(triggerPort, HIGH);
  delayMicroseconds(20);
  digitalWrite(triggerPort, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  long duration = pulseIn(echoPort, HIGH);
  // Calculating the distance
  long distance= duration*0.034/2;
  //Serial.println(distance);
  return distance;
}
