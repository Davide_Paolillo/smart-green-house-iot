#include "Arduino.h"
#include "DetectModality.h"

bool auto_mode;
bool manual_mode;

DetectModality::DetectModality (int led_1, int echo, int trigger, SoftwareSerial* bt) {
  this->led_1 = led_1;
  this->echo = echo;
  this->trigger = trigger;
  this->bt = bt;
  auto_mode = true;
  manual_mode = false;
  bs = new BlueToothSensorImpl(this->echo, this->trigger, this->bt);
  led = new Led(led_1);
}

void DetectModality::init(int period){
  Task::init(period);
}

void DetectModality::tick(){
  /// Se bluetooth e' available allora auto_mode = false, manual_mode = true
  if(bs->IsBlueToothAvailable()){
    led->switchOff();
    auto_mode = false;
    manual_mode = true;
  } else {
    led->switchOn();
    Serial.println(0);
  }
}

bool DetectModality::isMyTurn(){
  return auto_mode;
}
