#include "BlueToothSensorImpl.h"
#include "DistanceSensorImpl.h"
#include "SoftwareSerial.h"
#include "Arduino.h"

#define MSG_LENGTH 1

BlueToothSensorImpl::BlueToothSensorImpl(int echo, int trigger, SoftwareSerial* bt){
  this->echo = echo;
  this->trigger = trigger;
  ds = new DistanceSensorImpl(this->trigger, this->echo);
  bluetooth = bt;
}

bool BlueToothSensorImpl::IsBlueToothAvailable () {
  return bluetooth->available() && (ds->getSensorDistance() <= MIN_DIST);
}

String BlueToothSensorImpl::BlueToothMessage() {
  String res = "";
  for(int i = 0; i < MSG_LENGTH; i++){
    res += char(bluetooth->read());
  }
  return res;
}

bool BlueToothSensorImpl::BlueToothWrite(int out) {
  if(bluetooth->available()){
    bluetooth->println(out);
    return true;
  } else {
    return false;
  }
}
