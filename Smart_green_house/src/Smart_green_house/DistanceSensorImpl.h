#ifndef __DISTANCE_SENSOR_IMPL__
#define __DISTANCE_SENSOR_IMPL__

#include "DistanceSensor.h"

class DistanceSensorImpl: public DistanceSensor {

  public:
    DistanceSensorImpl(int triggerPort, int echoPort);
    long getSensorDistance();

  private:
    int triggerPort, echoPort;

};


#endif
