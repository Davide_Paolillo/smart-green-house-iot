#ifndef __MANUAL_MODE__
#define __MANUAL_MODE__

#include <Arduino.h>
#include <SoftwareSerial.h>
#include <ServoTimer2.h>
#include "BlueToothSensorImpl.h"
#include "Task.h"
#include "Led.h"

class ManualMode: public Task {
public:
  ManualMode(int led_3, int echo, int trigger, SoftwareSerial* bt, ServoTimer2* my_servo);
  void init(int period);
  void tick();
  bool isMyTurn();
private:
  Led* led;
  BlueToothSensorImpl* bs;
  int led_3;
  int echo, trigger;
  SoftwareSerial* bt;
  ServoTimer2* my_servo;
  String message;
};


#endif
