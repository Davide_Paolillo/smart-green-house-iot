#include "Led.h"
#include "Arduino.h"

Led::Led(int pin){
  this->pin = pin;
  pinMode(pin,OUTPUT);
}

void Led::switchOn(){
  digitalWrite(pin,HIGH);
}

void Led::switchOff(){
  digitalWrite(pin,LOW);
};

void Led::swithchOnWithIntensity(int intensity){
  intensity > 255 ? analogWrite(pin, 255) : intensity < 0 ? analogWrite(pin, 0) : analogWrite(pin, intensity);
}
