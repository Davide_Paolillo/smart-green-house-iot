#ifndef __DETECT_MOD__
#define __DETECT_MOD__

#include <Arduino.h>
#include "Task.h"
#include "Led.h"
#include "BlueToothSensorImpl.h"
#include "SoftwareSerial.h"

extern bool auto_mode;
extern bool manual_mode;

class DetectModality: public Task {
public:
  DetectModality(int led_1, int echo, int trigger, SoftwareSerial* bt);
  void init(int period);
  void tick();
  bool isMyTurn();
private:
  Led* led;
  BlueToothSensorImpl* bs;
  int led_1;
  int echo, trigger;
  SoftwareSerial* bt;
};

#endif
